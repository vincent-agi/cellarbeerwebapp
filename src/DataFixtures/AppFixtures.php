<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $vincent = new User();
        $vincent->setEmail("agi.v@hotmail.fr");
        $vincent->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        $vincent->setPassword('$argon2id$v=19$m=65536,t=4,p=1$ftOEFTgVGk/gpuQT7+xDZQ$QAGy+v/A9arvXKs62MH1nSHpsIE4vlDkBPUHMpFQztk');
        $vincent->setPrenom('Vincent');
        $vincent->setNom('AGI');
        $vincent->setDateNaissance(new \Datetime('1996-04-02'));
        $vincent->setPseudo('DictateurDeProjet');

        $manager->persist($vincent);

        $manager->flush();
    }
}
