<?php

namespace App\Repository;

use App\Entity\Cave;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Cave|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cave|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cave[]    findAll()
 * @method Cave[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CaveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cave::class);
    }

    /**
     * @return Cave[]
     */
      public function CaveByUser($userId)
      {
          $entityManager = $this->getEntityManager();

          $query = $entityManager->createQuery(
              'SELECT c
              FROM App\Entity\Cave c
              WHERE c.user =:userId
              ORDER BY c.nom ASC'
          )->setParameter('userId', $userId);

          // returns an array of Product objects
          return $query->getResult();
      }


    /*
    public function findOneBySomeField($value): ?Cave
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
