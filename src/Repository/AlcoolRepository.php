<?php

namespace App\Repository;

use App\Entity\Alcool;
use App\Entity\Cave;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Alcool|null find($id, $lockMode = null, $lockVersion = null)
 * @method Alcool|null findOneBy(array $criteria, array $orderBy = null)
 * @method Alcool[]    findAll()
 * @method Alcool[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlcoolRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Alcool::class);
    }

    /**
     * @return Alcool[]
     */
      public function AlcoolsByCave($caveId)
      {
          $entityManager = $this->getEntityManager();

          $query = $entityManager->createQuery(
              'SELECT a
              FROM App\Entity\Alcool a
              WHERE a.cave =:caveId
              ORDER BY a.nom ASC'
          )->setParameter('caveId', $caveId);

          // returns an array of Product objects
          return $query->getResult();
      }

      /**
       * @return Alcool[]
       */
        public function AlcoolsByUser($userId)
        {
            $entityManager = $this->getEntityManager();

            $query = $entityManager->createQuery(
                'SELECT a
                FROM App\Entity\Alcool a
                JOIN App\Entity\Cave c
                WHERE c.user =:userId
                ORDER BY a.note ASC'
            )->setParameter('userId', $userId);

            // returns an array of Product objects
            return $query->getResult();
        }

      /**
       * @return Alcool[]
       */
        public function top10()
        {
            $entityManager = $this->getEntityManager();

            $query = $entityManager->createQuery(
                'SELECT a
                FROM App\Entity\Alcool a
                ORDER BY a.note DESC'
            )->setMaxResults(10);

            // returns an array of Product objects
            return $query->getResult();
        }

    /*
    public function findOneBySomeField($value): ?Alcool
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
