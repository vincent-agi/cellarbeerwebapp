<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class CaveVoter extends Voter
{
    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['EDIT', 'DELETE', 'VIEW'])
            && $subject instanceof \App\Entity\Cave;
    }

    protected function voteOnAttribute($attribute, $cave, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'EDIT':
                return $user->getId() == $cave->getUser()->getId();
                break;
            case 'DELETE':
                return $user->getId() == $cave->getUser()->getId();
                break;
            case 'VIEW':
                return $user->getId() == $cave->getUser()->getId();
              break;
        }

        return false;
    }
}
