<?php

namespace App\Form;

use App\Entity\Alcool;
use App\Entity\Image;
use App\Entity\Cave;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class AlcoolType extends AbstractType
{
    private $tokenStorage;

  public function __construct(TokenStorageInterface $tokenStorage)
  {
    $this->tokenStorage = $tokenStorage;
  }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('type', ChoiceType::class, [
              'label'=>'Quel est le type de votre alcool ?',
              'choices'=> [
                'Brune'=>'Brune',
                'Blonde'=>'Blonde',
                'Blanche'=>'Blanche',
                'Ruby'=>'Ruby',
                'Triple'=>'Triple',
                'Autre'=>'Autre',
              ],
            ])
            ->add('degres')
            ->add('note', IntegerType::class, [
              'label'=>'Donner une note à cette boisson sur 20',
              'help'=>'Utilisez le . plutôt que la , pour la notation décimale',
            ])
            ->add('description')
            ->add('image', FileType::class, [
              'label'=> 'Sélectionner une image',
              'mapped'=>false,
              'required'=> false,
              'constraints' => [
                    new File([
                        'maxSize' => '10M',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                            'image/gif',
                        ],
                        'mimeTypesMessage' => 'Uploader un fichier de type JPEG, JPG, PNG ou GIF.',
            ]),]])
            ->add('cave', EntityType::class, [
              'class'=> Cave::class,
              'query_builder' => function ( EntityRepository $er ) {
                return $er->createQueryBuilder('c')
                ->where('c.user = :idUser')
                ->setParameter('idUser', $this->tokenStorage->getToken()->getUser()->getId());

              } ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Alcool::class,
        ]);
    }
}
