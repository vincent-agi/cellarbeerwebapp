<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200609051054 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE alcool ADD image_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE alcool ADD CONSTRAINT FK_E2D169FB3DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
        $this->addSql('CREATE INDEX IDX_E2D169FB3DA5256D ON alcool (image_id)');
        $this->addSql('ALTER TABLE image DROP FOREIGN KEY FK_C53D045F188DA4F');
        $this->addSql('DROP INDEX IDX_C53D045F188DA4F ON image');
        $this->addSql('ALTER TABLE image DROP alcools_id');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL, CHANGE pseudo pseudo VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cave CHANGE description description VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE alcool DROP FOREIGN KEY FK_E2D169FB3DA5256D');
        $this->addSql('DROP INDEX IDX_E2D169FB3DA5256D ON alcool');
        $this->addSql('ALTER TABLE alcool DROP image_id');
        $this->addSql('ALTER TABLE cave CHANGE description description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE image ADD alcools_id INT NOT NULL');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F188DA4F FOREIGN KEY (alcools_id) REFERENCES alcool (id)');
        $this->addSql('CREATE INDEX IDX_C53D045F188DA4F ON image (alcools_id)');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`, CHANGE pseudo pseudo VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
