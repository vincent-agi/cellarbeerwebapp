<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Alcool;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class BeerController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $user = $this->getUser();

        return $this->render('beer/index.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/top10", name="top_ten")
     */
    public function top_ten()
    {
        $user = $this->getUser();
        $top10 = $this->getDoctrine()->getRepository(Alcool::class)->top10();

        return $this->render('alcool/top10.html.twig', [
            'user' => $user,
            'alcools'=>$top10,
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, \Swift_Mailer $mailer)
    {
        $user = $this->getUser();
        $form = $this->createFormBuilder()
        ->add('nom', TextType::class, [
            'label'=>'Quel est ton nom ?',
            'required'=> true,
        ])
        ->add('prenom', TextType::class, [
            'label'=>'Quel est ton prénom ?',
            'required'=> true,
        ])
        ->add('email', EmailType::class, [
            'label'=>'Quelle est ton adresse email ?',
            'required'=> true,
        ])
            ->add('titre', TextType::class, [
                'label'=>'Titre du message',
                'required'=>true,
            ])
            ->add('message', TextareaType::class, [
                'label'=> 'Ton message',
                'required'=>true,
            ])
            ->add('send', SubmitType::class, ['label' => 'Envoyer'])
            ->getForm();;
            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // parametres du message
            $nom = $form->get('nom')->getData();;
            $prenom = $form->get('prenom')->getData();;
            $email = $form->get('email')->getData();
            if ($user) {
              $nom = $user->getNom();
              $prenom = $user->getPrenom();
              $email = $user->getEmail();
            }
            $leMessage = $form->get('message')->getData();
            $titre = $form->get('titre')->getData();
            // envoie du message
            $message = (new \Swift_Message("CellarBeer nouveau message"))
        ->setSubject($titre)
        ->setFrom($email)
        ->setTo('contact@followcarservice.fr')
        ->setBody("$nom $prenom vous a envoyé ce message : <br>
            $leMessage"
            ,'text/html'
        );

        $mailer->send($message);

        return $this->redirectToRoute('home', [
            'confirmation' => 'Votre message est bien partie',
            //'form' => $form->createView(),
        ]);
    }
        return $this->render('beer/contact.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
