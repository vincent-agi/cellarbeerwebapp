<?php

namespace App\Controller;

use App\Entity\Alcool;
use App\Form\AlcoolType;
use App\Repository\AlcoolRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile/alcool")
 */
class AlcoolController extends AbstractController
{
    /**
     * @Route("/", name="alcool_index", methods={"GET"})
     */
    public function index(AlcoolRepository $alcoolRepository): Response
    {

        return $this->render('alcool/listeByCave.html.twig', [
            'alcools' => $alcoolRepository->AlcoolsByUser($this->getUser()->getId()),
        ]);
    }

    /**
     * @Route("/new", name="alcool_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $alcool = new Alcool();
        $form = $this->createForm(AlcoolType::class, $alcool);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          //upload
            $alcoolFile = $form->get('image')->getData();
            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($alcoolFile) {
                $originalFilename = pathinfo($alcoolFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$alcoolFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $alcoolFile->move(
                        $this->getParameter('images_alcool_dir'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new \Exception("Error Processing Request $e", 1);

                }

                // updates the 'alcoolFilename' property to store the PDF file name
                // instead of its contents
                $alcool->setImage($newFilename);
            }
          //
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($alcool);
            $entityManager->flush();

            return $this->redirectToRoute('alcool_index');
        }

        return $this->render('alcool/new.html.twig', [
            'alcool' => $alcool,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="alcool_show", methods={"GET"})
     */
    public function show(Alcool $alcool): Response
    {
        return $this->render('alcool/show.html.twig', [
            'alcool' => $alcool,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="alcool_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Alcool $alcool): Response
    {
        $this->denyAccessUnlessGranted('EDIT', $alcool);
        $form = $this->createForm(AlcoolType::class, $alcool);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          $alcoolFile = $form->get('image')->getData();
          if ($alcoolFile) {
            $oldImage = $alcool->getImage();
            unlink($this->getParameter('images_alcool_dir').$oldImage);
            $originalFilename = pathinfo($alcoolFile->getClientOriginalName(), PATHINFO_FILENAME);
            // this is needed to safely include the file name as part of the URL
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $newFilename = $safeFilename.'-'.uniqid().'.'.$alcoolFile->guessExtension();

            // Move the file to the directory where brochures are stored
            try {
                $alcoolFile->move(
                    $this->getParameter('images_alcool_dir'),
                    $newFilename
                );
            } catch (FileException $e) {
                throw new \Exception("Error Processing Request $e", 1);

            }

            // updates the 'alcoolFilename' property to store the PDF file name
            // instead of its contents
            $alcool->setImage($newFilename);
          }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('alcool_index');
        }

        return $this->render('alcool/edit.html.twig', [
            'alcool' => $alcool,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="alcool_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Alcool $alcool): Response
    {
        $this->denyAccessUnlessGranted('DELETE', $alcool);
        if ($this->isCsrfTokenValid('delete'.$alcool->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($alcool);
            $entityManager->flush();
        }

        return $this->redirectToRoute('alcool_index');
    }

    /**
     * @Route("/alcools/cave/{id}", name="voirAlcools")
     */
    public function voirAlcools(int $id)
    {
        $alcools = $this->getDoctrine()->getRepository(Alcool::class)->AlcoolsByCave($id);

        return $this->render('alcool/listeByCave.html.twig', [
          'alcools'=>$alcools
        ]);
    }
}
