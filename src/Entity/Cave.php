<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CaveRepository")
 */
class Cave
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="caves")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Alcool", mappedBy="cave", orphanRemoval=true)
     */
    private $alcools;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    public function __construct()
    {
        $this->alcools = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Alcool[]
     */
    public function getAlcools(): Collection
    {
        return $this->alcools;
    }

    public function addAlcool(Alcool $alcool): self
    {
        if (!$this->alcools->contains($alcool)) {
            $this->alcools[] = $alcool;
            $alcool->setCave($this);
        }

        return $this;
    }

    public function removeAlcool(Alcool $alcool): self
    {
        if ($this->alcools->contains($alcool)) {
            $this->alcools->removeElement($alcool);
            // set the owning side to null (unless already changed)
            if ($alcool->getCave() === $this) {
                $alcool->setCave(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function __toString()
    {
      return $this->getNom();
    }
}
